import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class UserForm extends StatefulWidget {
  String userId;
  UserForm({Key? key, required this.userId}) : super(key: key);

  @override
  _UserFormState createState() => _UserFormState(this.userId);
}

class _UserFormState extends State<UserForm> {
  String userId;
  String fullname = "";
  String company = "";
  String age = "0";
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  _UserFormState(this.userId);
  TextEditingController _fullNameControler = new TextEditingController();
  TextEditingController _companyControler = new TextEditingController();
  TextEditingController _ageControler = new TextEditingController();

  @override
  void initState() {
    super.initState();
    if (this.userId.isNotEmpty) {
      users.doc(this.userId).get().then((snapshot) {
        if (snapshot.exists) {
          print(snapshot.data());
          var data = snapshot.data() as Map<String, dynamic>;

          fullname = data['full_name'];
          company = data['company'];
          age = data['age'];
          _fullNameControler.text = fullname;
          _companyControler.text = company;
          _ageControler.text = age;
        }
      });
    }
  }

  final _formKey = GlobalKey<FormState>();

  Future<void> addUser() {
    return users
        .add({
          'full_name': this.userId,
          'company': this.company,
          'age': int.parse(age)
        })
        .then((value) => print("User Added"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  Future<void> updateUser() {
    return users
        .doc(this.userId)
        .update({
          'company': company,
          "full_name": this.fullname,
          'age': int.parse(age)
        })
        .then((value) => print("User Updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('User'),
      ),
      body: Form(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                controller: _fullNameControler,
                decoration: InputDecoration(labelText: 'Full Name'),
                onChanged: (value) {
                  setState(() {
                    fullname = value;
                  });
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Please input Name";
                  }
                  return null;
                },
              ),
              TextFormField(
                controller: _companyControler,
                decoration: InputDecoration(labelText: 'company'),
                onChanged: (value) {
                  setState(() {
                    company = value;
                  });
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Please input Company";
                  }
                  return null;
                },
              ),
              TextFormField(
                controller: _ageControler,
                decoration: InputDecoration(labelText: 'age'),
                onChanged: (value) {
                  setState(() {
                    age = value;
                  });
                },
                validator: (value) {
                  if (value == null ||
                      value.isEmpty ||
                      int.tryParse(value) == null) {
                    return "Please input Age";
                  }
                  return null;
                },
              ),
              ElevatedButton(
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    if (userId.isNotEmpty) {
                      await updateUser();
                    } else {
                      await addUser();
                    }

                    Navigator.pop(context);
                  }
                },
                child: Text('Save'),
              )
            ],
          )),
    );
  }
}
